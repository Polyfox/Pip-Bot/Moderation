package eu.polyfox.pipbot.moderation;

import eu.polyfox.pipbot.plugin.Plugin;
import lombok.extern.slf4j.Slf4j;

/**
 * Plugin's main class.
 * @author traxam
 */
@Slf4j
public class ModerationPlugin extends Plugin {
    @Override
    protected void enable() {
        log.info("Enabled moderation.");
    }

    @Override
    protected void disable() {
        log.info("Disabled moderation.");
    }
}
